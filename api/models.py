from main import db


class Book(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    author = db.Column(db.String)
    title = db.Column(db.String)
    completed = db.Column(db.Boolean, default=False)
    pagesRead = db.Column(db.Integer, default=0)

    def toDict(self):
        return {
            'id': self.id,
            'author': self.author,
            'title': self.title,
            'completed': self.completed,
            'pagesRead': self.pagesRead
        }
