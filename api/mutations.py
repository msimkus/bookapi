from api import db
from api.models import Book


def resolveCreateBook(obj, info, author, title):
    try:
        book = Book(
            author=author,
            title=title
        )
        db.session.add(book)
        db.session.commit()
        payload = {
            'success': True,
            'book': book.toDict()
        }
    except Exception as error:
        payload = {
            'success': False,
            'errors': [str(error)]
        }

    return payload


def resolveMarkAsRead(obj, info, bookId):
    try:
        book = Book.query.get(bookId)
        book.completed = True
        db.session.add(book)
        db.session.commit()
        payload = {
            'success': True,
            'book': book.toDict()
        }
    except AttributeError:  # book not found
        payload = {
            'success': False,
            'errors': [f'Book matching id {bookId} was not found']
        }

    return payload


def resolveDeleteBook(obj, info, bookId):
    try:
        book = Book.query.get(bookId)
        db.session.delete(book)
        db.session.commit()
        payload = {'success': True}
    except AttributeError:
        payload = {
            'success': False,
            'errors': [f'Book matching id {bookId} not found']
        }

    return payload


def resolveUpdateReadingProgress(obj, info, bookId, newPagesRead):
    try:
        book = Book.query.get(bookId)
        book.pagesRead = newPagesRead
        db.session.add(book)
        db.session.commit()
        payload = {
            'success': True,
            'book': book.toDict()
        }
    except AttributeError:  # book not found
        payload = {
            'success': False,
            'errors': [f'Book matching id {bookId} not found']
        }

    return payload
