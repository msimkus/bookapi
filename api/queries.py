from .models import Book


def resolveBooks(obj, info):
    try:
        books = [book.toDict() for book in Book.query.all()]
        payload = {
            'success': True,
            'books': books
        }
    except Exception as error:
        payload = {
            'success': False,
            'errors': [str(error)]
        }
    return payload


def resolveBook(obj, info, bookId):
    try:
        book = Book.query.get(bookId)
        payload = {
            'success': True,
            'book': book.toDict()
        }
    except AttributeError:  # book not found
        payload = {
            'success': False,
            'errors': [f'Book item matching id {bookId} not found']
        }

    return payload
