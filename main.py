from api import app, db
from api import models
from ariadne import load_schema_from_path, make_executable_schema, \
    graphql_sync, ObjectType
from ariadne.constants import PLAYGROUND_HTML
from flask import request, jsonify
from api.queries import resolveBooks, resolveBook
from api.mutations import resolveCreateBook, resolveMarkAsRead, \
    resolveDeleteBook, resolveUpdateReadingProgress


query = ObjectType('Query')
query.set_field('books', resolveBooks)
query.set_field('book', resolveBook)

mutation = ObjectType('Mutation')
mutation.set_field('createBook', resolveCreateBook)
mutation.set_field('markAsRead', resolveMarkAsRead)
mutation.set_field('deleteBook', resolveDeleteBook)
mutation.set_field('updateReadingProgress', resolveUpdateReadingProgress)

type_defs = load_schema_from_path('schema.graphql')
schema = make_executable_schema(
    type_defs,
    query,
    mutation
)


@app.route('/graphql', methods=['GET'])
def graphqlPlayground():
    return PLAYGROUND_HTML, 200


@app.route('/graphql', methods=['POST'])
def graphqlServer():
    data = request.get_json()

    success, result = graphql_sync(
        schema,
        data,
        context_value=request,
        debug=app.debug
    )

    status_code = 200 if success else 400

    return jsonify(result), status_code
